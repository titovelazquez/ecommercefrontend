import React, { Component } from "react";
import InformacionPez from "./InformacionPez/InformacionPez";
import "./Inicio.css";
import Jacintas from "./Jacintas";

export class Inicio extends Component {
  render() {
    return (
      <div>
        <InformacionPez/>
        <div class="navigation">
          <div className="container">
            <div className="row">
              {/* Peces */}
              <span className="col-sm-6">
                <img
                  className="rounded float-left"
                  src={require("../../resources/images/betta2.gif")}
                />
              </span>
              <span className="col-sm-6">
                <img
                  className="rounded float-right"
                  src={require("../../resources/images/betta3.gif")}
                />
              </span>
            </div>
          </div>
          <Jacintas />
        </div>
      </div>
    );
  }
}
