import React, { Component } from 'react';

class Link extends Component{

    constructor(props) {
        super(props);
        this.state = {
            pez: Array
        };
    }

    muestra = async (e) => {
        const api_fetch = await fetch(
          'http://localhost:8080/peces/getPecesData'
        );
        const pez = await api_fetch.json();
        this.setState(state => ({
            pez: pez.map((pez) =>  <ul>
                <li key={pez.idPez}>id pez: {pez.idPez}</li>
                <li >nombre pez: {pez.nombrePez}</li>
                <li>stock: {pez.stock}</li>
                <li>informacion pez: {pez.infoPez}</li>
                <li>precio pez: {pez.precioPez}</li>
                <li>image: {pez.image}</li>
                </ul>)
        }));
    }

    render(){

        return(
            <div>
                <div>
                    <h4>Presiona el boton de abajo para obtener un pez</h4>
                    <button onClick={this.muestra}>Buscar</button>
                </div>
                <div>{this.state.pez}</div>
            </div>
        );
    }
};

export default Link;