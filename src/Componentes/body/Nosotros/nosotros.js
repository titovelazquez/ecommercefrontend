import React, { Component } from "react";
import {Map, InfoWindow, Marker, GoogleApiWrapper} from 'google-maps-react';
import './nosotros.css';

export class Nosotros extends Component {
  render() {
    return (
      <div id="FondoNosotros1">
        <br/>
        <h2 className="title-nosotros">Nosotros</h2>
        <hr/>
        <section className="ae-container-fluid ae-container-fluid--inner rk-portfolio">
          <div className="ae-masonry ae-masonry-md-2 ae-masonry-xl-4">
            <a className="rk-item ae-masonry__item">
              <img src={require("../../../resources/images/Nosotros/Pablo.jpg")} class="img-fluid" alt="Responsive image"/>
              <div className="item-meta">
                <h2> Pablo Orozco</h2>
                <p id="formato1">
                  {" "}
                  El artista del grupo. Su pasión por la armonía de color y la
                  congruencia en la paleta de colores nos dan el plus que sólo
                  un fotógrafo profesional podría.{" "}
                </p>
              </div>
            </a>
            <a className="rk-item ae-masonry__item">
              <img src={require("../../../resources/images/Nosotros/tito.jpg")} class="img-fluid" alt="Responsive image"/>
              <div className="item-meta">
                <h2> Tito Velázquez</h2>
                <p id="formato1">
                  Cuenta con el baggage técnico derivado de años de experiencia
                  en el desarrollo web. Su pasión por el ecommerce así como por
                  la seguridad en internet lo hacen un socio clave para la
                  empresa.{" "}
                </p>
              </div>
            </a>
            <a className="rk-item ae-masonry__item">
              <img src={require("../../../resources/images/Nosotros/Pau.jpg")} class="img-fluid" alt="Responsive image"/>
              <div className="item-meta">
                <h2>Pau Fregoso</h2>
                <p id="formato1">
                  Su energía y fuerte temperamento proveen una atmósfera de
                  dinamismo. Su visión para los negocios así como su comprensión
                  técnia la hacen una socia ideal.{" "}
                </p>
              </div>
            </a>
            <a className="rk-item ae-masonry__item">
              <img src={require("../../../resources/images/Nosotros/adrian.jpg")} class="img-fluid" alt="Responsive image"/>
              <div className="item-meta">
                <h2>Adrian Betanzos</h2>
                <p id="formato1">
                  Ingeniero de profesión. Amante de la naturaleza y de la
                  elegancia de la creación. Sus habiildades administrativas y
                  trato con los clientes lo definen.{" "}
                </p>
              </div>
            </a>
            <a className="rk-item ae-masonry__item">
              <img src={require("../../../resources/images/Nosotros/luis.jpg")} class="img-fluid" alt="Responsive image"/>
              <div className="item-meta">
                <h2> Luis Fernando Guevara </h2>
                <p id="formato1">
                  Programador entusiasta. Su paciencia y atención al detalle lo
                  hacen ideal para lograr fijarse en aspectos que los demás
                  pasarían por alto. Sus habilidades técnicas lo respaldan.
                  Coder de noche, ingeniero 24/7.{" "}
                </p>
              </div>
            </a>
          </div>
        </section>

        <Map google={this.props.google}></Map>

      </div>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: ("AIzaSyApt_hLvX-4hDlaa6oiRjW69otm_aA81po")
})(Nosotros)
