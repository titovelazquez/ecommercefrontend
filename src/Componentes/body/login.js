import React, { Component } from 'react';

export class Login extends Component {

    alEnviarLogin = e =>{
        e.preventDefault();
        alert("Usuario Loggeado");
    }

    render(){
        return (
            <form onSubmit={this.alEnviarLogin} className="login-wrap form-group">
                <div className="login-html">
                    <input id="tab-1" type="radio" name="tab" className="sign-in" defaultChecked/>
                    <label htmlFor="tab-1" className="tab">Sign In</label>
                    <input id="tab-2" type="radio" name="tab" className="sign-up"/>
                    <label htmlFor="tab-2" className="tab">Sign Up</label>
                    <div className="login-form">
                        <div className="sign-in-htm">
                            <div className="group">
                                <label htmlFor="user" className="label">Username</label>
                                <input id="sign_in_user" type="text" className="input"/>
                            </div>
                            <div className="group">
                                <label htmlFor="pass" className="label">Password</label>
                                <input id="sign_in_pass" type="password" className="input" data-type="password"/>
                            </div>
                            <div className="group">
                                <input id="check" type="checkbox" className="check" defaultChecked/>
                                <label htmlFor="check"><span className="icon"></span> Keep me Signed in</label>
                            </div>
                            <div className="group">
                                <input type="submit" className="button" value="Sign In"/>
                            </div>
                            <div className="hr"></div>
                            <div className="foot-lnk">
                                <a href="#forgot">Forgot Password?</a>
                            </div>
                        </div>
                        <div className="sign-up-htm">
                            <div className="group">
                                <label htmlFor="user" className="label">Username</label>
                                <input id="user" type="text" className="input"/>
                            </div>
                            <div className="group">
                                <label htmlFor="name" className="label">Name</label>
                                <input id="name" type="text" className="input"/>
                            </div>
                            <div className="group">
                                <label htmlFor="surname" className="label">Surname</label>
                                <input id="surname" type="text" className="input" data-type="surname"/>
                            </div>
                            <div className="group">
                                <label htmlFor="password" className="label">Password</label>
                                <input id="password" type="password" className="input" data-type="password"/>
                            </div>
                            <div className="group">
                                <label htmlFor="repasrword" className="label">Repeat Password</label>
                                <input id="repasrword" type="password" className="input" data-type="password"/>
                            </div>
                            <div className="group">
                                <label htmlFor="email" className="label">Email Address</label>
                                <input id="email" type="text" className="input"/>
                            </div>
                            <div className="group">
                                <input type="submit" className="button" value="Sign Up"/>
                            </div>
                            <div className="hr"></div>
                            <div className="foot-lnk">
                                <a htmlFor="tab-1">Already Member?</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        );
    }
};
export default Login;