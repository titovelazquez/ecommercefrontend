import React, { Component } from 'react';

class Jacintas extends Component {

    constructor(props) {
        super(props);
        this.state = {
            seconds: 0,
            image: require('../../resources/images/Jacintas/1.jpg'),
            jacintas: [1, 2, 3]
        };
    }
    tick() {
        this.setState(state => ({
            seconds: state.seconds + 1,
            image: require('../../resources/images/Jacintas/' + state.jacintas[this.state.seconds] + '.jpg')
        }));
        
        if (this.state.seconds > 2) {
            this.state.seconds = 0;
        }
    }
    componentDidMount() {
        this.interval = setInterval(() => this.tick(), 3000);
    }

    render() {
        return (
            <div>
                <div>
                    <div className="behind">
                        <img src={this.state.image} />
                    </div>
                </div>
            </div>
        );
    }
}

export default Jacintas;