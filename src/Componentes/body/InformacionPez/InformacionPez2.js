import React, { Component } from "react";
//import import "../body/PecesEnVenta/PecesEnVenta";

export class InformacionPez2 extends Component {
  render() {
    const valor = {
      display: "none"
    };

    return (
      <div>
        <div id="2" style={valor}>
          <div class="ae-grid__item item-lg-8 item-lg--offset-2">
            <h4 class="ae-u-bolder">pez 1</h4>
            <p class="ae-eta">
              Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
              officia deserunt mollit anim id est laborum.
            </p>
            <p class="ae-eta">
              Sed ut perspiciatis unde omnis iste natus error sit voluptatem
              accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
              quae ab illo inventore veritatis.
            </p>
            <div>
              {" "}
              <img
                src={require("../../../resources/images/descarga (2).jpg")}
                alt="Urku Portfolio"
              />
            </div>
          </div>

          <hr />
          <h3>Peces en venta xoxoxo</h3>

          <br />

          <div class="ae-grid--alt test">
            <div class="ae-grid__item--alt item-sm--auto item-sm--offset-1 item-lg--offset-0 item-sm--end item-sm--bottom">
              <div class="ae-grid ae-grid--alt8 au-xs-ta-center au-sm-ta-left">
                <div class="ae-grid__item--alt8 item-lg-6">
                  <span class="ae-u-boldest">1.</span>
                  <p class="au-mt-1">
                    Suspendisse ultricies tellus eget nisl molestie, quis
                    sagittis mauris placerat.
                  </p>
                  <span class="ae-u-boldest">2.</span>
                  <p class="au-mt-1">
                    Pellentesque et nulla pulvinar, bibendum quam ac, cursus
                    turpis.
                  </p>
                </div>
                <div class="ae-grid__item--alt8 item-lg-6">
                  <span class="ae-u-boldest">3.</span>
                  <p class="au-mt-1">
                    Sed quam odio, blandit sit amet dapibus id, aliquet
                    sollicitudin enim.{" "}
                  </p>
                  <span class="ae-u-boldest">4.</span>
                  <p class="au-mt-1">
                    Morbi hendrerit laoreet lectus a fringilla.
                  </p>
                </div>
              </div>
            </div>
            <div class="ae-grid__item--alt item-sm--auto item-lg-3 item-lg--offset-1 item-lg--end">
              <img
                src={require("../../../resources/images/descarga (2).jpg")}
                alt="Urku Portfolio"
              />
            </div>
          </div>

          <div class="ae-container-fluid au-pt-4 au-pb-4">
            <div class="group-buttons">
              <a href="/" class="au-mt-2 arrow-button arrow-button--center">
                COMPRAR
                <span class="arrow-cont">
                  <svg>
                    <use href="assets/img/symbols.svg#arrow" />
                  </svg>
                </span>
              </a>
            </div>
          </div>
        </div>

        <div id="3" style={valor}>
          <div class="ae-grid__item item-lg-8 item-lg--offset-2">
            <h4 class="ae-u-bolder">pez 2</h4>
            <p class="ae-eta">
              Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
              officia deserunt mollit anim id est laborum.
            </p>
            <p class="ae-eta">
              Sed ut perspiciatis unde omnis iste natus error sit voluptatem
              accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
              quae ab illo inventore veritatis.
            </p>
            <div>
              {" "}
              <img
                src={require("../../../resources/images/descarga (2).jpg")}
                alt="Urku Portfolio"
              />
            </div>
          </div>

          <hr />
          <h3>Peces en venta xoxoxo</h3>

          <br />

          <div class="ae-grid--alt test">
            <div class="ae-grid__item--alt item-sm--auto item-sm--offset-1 item-lg--offset-0 item-sm--end item-sm--bottom">
              <div class="ae-grid ae-grid--alt8 au-xs-ta-center au-sm-ta-left">
                <div class="ae-grid__item--alt8 item-lg-6">
                  <span class="ae-u-boldest">1.</span>
                  <p class="au-mt-1">
                    Suspendisse ultricies tellus eget nisl molestie, quis
                    sagittis mauris placerat.
                  </p>
                  <span class="ae-u-boldest">2.</span>
                  <p class="au-mt-1">
                    Pellentesque et nulla pulvinar, bibendum quam ac, cursus
                    turpis.
                  </p>
                </div>
                <div class="ae-grid__item--alt8 item-lg-6">
                  <span class="ae-u-boldest">3.</span>
                  <p class="au-mt-1">
                    Sed quam odio, blandit sit amet dapibus id, aliquet
                    sollicitudin enim.{" "}
                  </p>
                  <span class="ae-u-boldest">4.</span>
                  <p class="au-mt-1">
                    Morbi hendrerit laoreet lectus a fringilla.
                  </p>
                </div>
              </div>
            </div>
            <div class="ae-grid__item--alt item-sm--auto item-lg-3 item-lg--offset-1 item-lg--end">
              <img
                src={require("../../../resources/images/descarga (2).jpg")}
                alt="Urku Portfolio"
              />
            </div>
          </div>

          <div class="ae-container-fluid au-pt-4 au-pb-4">
            <div class="group-buttons">
              <a href="/" class="au-mt-2 arrow-button arrow-button--center">
                COMPRAR
                <span class="arrow-cont">
                  <svg>
                    <use href="assets/img/symbols.svg#arrow" />
                  </svg>
                </span>
              </a>
            </div>
          </div>
        </div>

        <div style={valor}>
          <div class="ae-grid__item item-lg-8 item-lg--offset-2">
            <h4 class="ae-u-bolder">pez 3</h4>
            <p class="ae-eta">
              Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
              officia deserunt mollit anim id est laborum.
            </p>
            <p class="ae-eta">
              Sed ut perspiciatis unde omnis iste natus error sit voluptatem
              accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
              quae ab illo inventore veritatis.
            </p>
            <div>
              {" "}
              <img
                src={require("../../../resources/images/descarga (2).jpg")}
                alt="Urku Portfolio"
              />
            </div>
          </div>

          <hr />
          <h3>Peces en venta xoxoxo</h3>

          <br />

          <div class="ae-grid--alt test">
            <div class="ae-grid__item--alt item-sm--auto item-sm--offset-1 item-lg--offset-0 item-sm--end item-sm--bottom">
              <div class="ae-grid ae-grid--alt8 au-xs-ta-center au-sm-ta-left">
                <div class="ae-grid__item--alt8 item-lg-6">
                  <span class="ae-u-boldest">1.</span>
                  <p class="au-mt-1">
                    Suspendisse ultricies tellus eget nisl molestie, quis
                    sagittis mauris placerat.
                  </p>
                  <span class="ae-u-boldest">2.</span>
                  <p class="au-mt-1">
                    Pellentesque et nulla pulvinar, bibendum quam ac, cursus
                    turpis.
                  </p>
                </div>
                <div class="ae-grid__item--alt8 item-lg-6">
                  <span class="ae-u-boldest">3.</span>
                  <p class="au-mt-1">
                    Sed quam odio, blandit sit amet dapibus id, aliquet
                    sollicitudin enim.{" "}
                  </p>
                  <span class="ae-u-boldest">4.</span>
                  <p class="au-mt-1">
                    Morbi hendrerit laoreet lectus a fringilla.
                  </p>
                </div>
              </div>
            </div>
            <div class="ae-grid__item--alt item-sm--auto item-lg-3 item-lg--offset-1 item-lg--end">
              <img
                src={require("../../../resources/images/descarga (2).jpg")}
                alt="Urku Portfolio"
              />
            </div>
          </div>

          <div class="ae-container-fluid au-pt-4 au-pb-4">
            <div class="group-buttons">
              <a href="/" class="au-mt-2 arrow-button arrow-button--center">
                COMPRAR
                <span class="arrow-cont">
                  <svg>
                    <use href="assets/img/symbols.svg#arrow" />
                  </svg>
                </span>
              </a>
            </div>
          </div>
        </div>

        <div style={valor}>
          <div class="ae-grid__item item-lg-8 item-lg--offset-2">
            <h4 class="ae-u-bolder">pez 4</h4>
            <p class="ae-eta">
              Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
              officia deserunt mollit anim id est laborum.
            </p>
            <p class="ae-eta">
              Sed ut perspiciatis unde omnis iste natus error sit voluptatem
              accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
              quae ab illo inventore veritatis.
            </p>
            <div>
              {" "}
              <img
                src={require("../../../resources/images/descarga (2).jpg")}
                alt="Urku Portfolio"
              />
            </div>
          </div>

          <hr />
          <h3>Peces en venta xoxoxo</h3>

          <br />

          <div class="ae-grid--alt test">
            <div class="ae-grid__item--alt item-sm--auto item-sm--offset-1 item-lg--offset-0 item-sm--end item-sm--bottom">
              <div class="ae-grid ae-grid--alt8 au-xs-ta-center au-sm-ta-left">
                <div class="ae-grid__item--alt8 item-lg-6">
                  <span class="ae-u-boldest">1.</span>
                  <p class="au-mt-1">
                    Suspendisse ultricies tellus eget nisl molestie, quis
                    sagittis mauris placerat.
                  </p>
                  <span class="ae-u-boldest">2.</span>
                  <p class="au-mt-1">
                    Pellentesque et nulla pulvinar, bibendum quam ac, cursus
                    turpis.
                  </p>
                </div>
                <div class="ae-grid__item--alt8 item-lg-6">
                  <span class="ae-u-boldest">3.</span>
                  <p class="au-mt-1">
                    Sed quam odio, blandit sit amet dapibus id, aliquet
                    sollicitudin enim.{" "}
                  </p>
                  <span class="ae-u-boldest">4.</span>
                  <p class="au-mt-1">
                    Morbi hendrerit laoreet lectus a fringilla.
                  </p>
                </div>
              </div>
            </div>
            <div class="ae-grid__item--alt item-sm--auto item-lg-3 item-lg--offset-1 item-lg--end">
              <img
                src={require("../../../resources/images/descarga (2).jpg")}
                alt="Urku Portfolio"
              />
            </div>
          </div>

          <div class="ae-container-fluid au-pt-4 au-pb-4">
            <div class="group-buttons">
              <a href="/" class="au-mt-2 arrow-button arrow-button--center">
                COMPRAR
                <span class="arrow-cont">
                  <svg>
                    <use href="assets/img/symbols.svg#arrow" />
                  </svg>
                </span>
              </a>
            </div>
          </div>
        </div>

        <div style={valor}>
          <div class="ae-grid__item item-lg-8 item-lg--offset-2">
            <h4 class="ae-u-bolder">pez 5</h4>
            <p class="ae-eta">
              Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
              officia deserunt mollit anim id est laborum.
            </p>
            <p class="ae-eta">
              Sed ut perspiciatis unde omnis iste natus error sit voluptatem
              accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
              quae ab illo inventore veritatis.
            </p>
            <div>
              {" "}
              <img
                src={require("../../../resources/images/descarga (2).jpg")}
                alt="Urku Portfolio"
              />
            </div>
          </div>

          <hr />
          <h3>Peces en venta xoxoxo</h3>

          <br />

          <div class="ae-grid--alt test">
            <div class="ae-grid__item--alt item-sm--auto item-sm--offset-1 item-lg--offset-0 item-sm--end item-sm--bottom">
              <div class="ae-grid ae-grid--alt8 au-xs-ta-center au-sm-ta-left">
                <div class="ae-grid__item--alt8 item-lg-6">
                  <span class="ae-u-boldest">1.</span>
                  <p class="au-mt-1">
                    Suspendisse ultricies tellus eget nisl molestie, quis
                    sagittis mauris placerat.
                  </p>
                  <span class="ae-u-boldest">2.</span>
                  <p class="au-mt-1">
                    Pellentesque et nulla pulvinar, bibendum quam ac, cursus
                    turpis.
                  </p>
                </div>
                <div class="ae-grid__item--alt8 item-lg-6">
                  <span class="ae-u-boldest">3.</span>
                  <p class="au-mt-1">
                    Sed quam odio, blandit sit amet dapibus id, aliquet
                    sollicitudin enim.{" "}
                  </p>
                  <span class="ae-u-boldest">4.</span>
                  <p class="au-mt-1">
                    Morbi hendrerit laoreet lectus a fringilla.
                  </p>
                </div>
              </div>
            </div>
            <div class="ae-grid__item--alt item-sm--auto item-lg-3 item-lg--offset-1 item-lg--end">
              <img
                src={require("../../../resources/images/descarga (2).jpg")}
                alt="Urku Portfolio"
              />
            </div>
          </div>

          <div class="ae-container-fluid au-pt-4 au-pb-4">
            <div class="group-buttons">
              <a href="/" class="au-mt-2 arrow-button arrow-button--center">
                COMPRAR
                <span class="arrow-cont">
                  <svg>
                    <use href="assets/img/symbols.svg#arrow" />
                  </svg>
                </span>
              </a>
            </div>
          </div>
        </div>

        <div style={valor}>
          <div class="ae-grid__item item-lg-8 item-lg--offset-2">
            <h4 class="ae-u-bolder">pez 6</h4>
            <p class="ae-eta">
              Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
              officia deserunt mollit anim id est laborum.
            </p>
            <p class="ae-eta">
              Sed ut perspiciatis unde omnis iste natus error sit voluptatem
              accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
              quae ab illo inventore veritatis.
            </p>
            <div>
              {" "}
              <img
                src={require("../../../resources/images/descarga (2).jpg")}
                alt="Urku Portfolio"
              />
            </div>
          </div>

          <hr />
          <h3>Peces en venta xoxoxo</h3>

          <br />

          <div class="ae-grid--alt test">
            <div class="ae-grid__item--alt item-sm--auto item-sm--offset-1 item-lg--offset-0 item-sm--end item-sm--bottom">
              <div class="ae-grid ae-grid--alt8 au-xs-ta-center au-sm-ta-left">
                <div class="ae-grid__item--alt8 item-lg-6">
                  <span class="ae-u-boldest">1.</span>
                  <p class="au-mt-1">
                    Suspendisse ultricies tellus eget nisl molestie, quis
                    sagittis mauris placerat.
                  </p>
                  <span class="ae-u-boldest">2.</span>
                  <p class="au-mt-1">
                    Pellentesque et nulla pulvinar, bibendum quam ac, cursus
                    turpis.
                  </p>
                </div>
                <div class="ae-grid__item--alt8 item-lg-6">
                  <span class="ae-u-boldest">3.</span>
                  <p class="au-mt-1">
                    Sed quam odio, blandit sit amet dapibus id, aliquet
                    sollicitudin enim.{" "}
                  </p>
                  <span class="ae-u-boldest">4.</span>
                  <p class="au-mt-1">
                    Morbi hendrerit laoreet lectus a fringilla.
                  </p>
                </div>
              </div>
            </div>
            <div class="ae-grid__item--alt item-sm--auto item-lg-3 item-lg--offset-1 item-lg--end">
              <img
                src={require("../../../resources/images/descarga (2).jpg")}
                alt="Urku Portfolio"
              />
            </div>
          </div>

          <div class="ae-container-fluid au-pt-4 au-pb-4">
            <div class="group-buttons">
              <a href="/" class="au-mt-2 arrow-button arrow-button--center">
                COMPRAR
                <span class="arrow-cont">
                  <svg>
                    <use href="assets/img/symbols.svg#arrow" />
                  </svg>
                </span>
              </a>
            </div>
          </div>
        </div>

        <div style={valor}>
          <div class="ae-grid__item item-lg-8 item-lg--offset-2">
            <h4 class="ae-u-bolder">pez 7</h4>
            <p class="ae-eta">
              Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
              officia deserunt mollit anim id est laborum.
            </p>
            <p class="ae-eta">
              Sed ut perspiciatis unde omnis iste natus error sit voluptatem
              accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
              quae ab illo inventore veritatis.
            </p>
            <div>
              {" "}
              <img
                src={require("../../../resources/images/descarga (2).jpg")}
                alt="Urku Portfolio"
              />
            </div>
          </div>

          <hr />
          <h3>Peces en venta xoxoxo</h3>

          <br />

          <div class="ae-grid--alt test">
            <div class="ae-grid__item--alt item-sm--auto item-sm--offset-1 item-lg--offset-0 item-sm--end item-sm--bottom">
              <div class="ae-grid ae-grid--alt8 au-xs-ta-center au-sm-ta-left">
                <div class="ae-grid__item--alt8 item-lg-6">
                  <span class="ae-u-boldest">1.</span>
                  <p class="au-mt-1">
                    Suspendisse ultricies tellus eget nisl molestie, quis
                    sagittis mauris placerat.
                  </p>
                  <span class="ae-u-boldest">2.</span>
                  <p class="au-mt-1">
                    Pellentesque et nulla pulvinar, bibendum quam ac, cursus
                    turpis.
                  </p>
                </div>
                <div class="ae-grid__item--alt8 item-lg-6">
                  <span class="ae-u-boldest">3.</span>
                  <p class="au-mt-1">
                    Sed quam odio, blandit sit amet dapibus id, aliquet
                    sollicitudin enim.{" "}
                  </p>
                  <span class="ae-u-boldest">4.</span>
                  <p class="au-mt-1">
                    Morbi hendrerit laoreet lectus a fringilla.
                  </p>
                </div>
              </div>
            </div>
            <div class="ae-grid__item--alt item-sm--auto item-lg-3 item-lg--offset-1 item-lg--end">
              <img
                src={require("../../../resources/images/descarga (2).jpg")}
                alt="Urku Portfolio"
              />
            </div>
          </div>

          <div class="ae-container-fluid au-pt-4 au-pb-4">
            <div class="group-buttons">
              <a href="/" class="au-mt-2 arrow-button arrow-button--center">
                COMPRAR
                <span class="arrow-cont">
                  <svg>
                    <use href="assets/img/symbols.svg#arrow" />
                  </svg>
                </span>
              </a>
            </div>
          </div>
        </div>

        <div style={valor}>
          <div class="ae-grid__item item-lg-8 item-lg--offset-2">
            <h4 class="ae-u-bolder">pez 8</h4>
            <p class="ae-eta">
              Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
              officia deserunt mollit anim id est laborum.
            </p>
            <p class="ae-eta">
              Sed ut perspiciatis unde omnis iste natus error sit voluptatem
              accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
              quae ab illo inventore veritatis.
            </p>
            <div>
              {" "}
              <img
                src={require("../../../resources/images/descarga (2).jpg")}
                alt="Urku Portfolio"
              />
            </div>
          </div>

          <hr />
          <h3>Peces en venta xoxoxo</h3>

          <br />

          <div class="ae-grid--alt test">
            <div class="ae-grid__item--alt item-sm--auto item-sm--offset-1 item-lg--offset-0 item-sm--end item-sm--bottom">
              <div class="ae-grid ae-grid--alt8 au-xs-ta-center au-sm-ta-left">
                <div class="ae-grid__item--alt8 item-lg-6">
                  <span class="ae-u-boldest">1.</span>
                  <p class="au-mt-1">
                    Suspendisse ultricies tellus eget nisl molestie, quis
                    sagittis mauris placerat.
                  </p>
                  <span class="ae-u-boldest">2.</span>
                  <p class="au-mt-1">
                    Pellentesque et nulla pulvinar, bibendum quam ac, cursus
                    turpis.
                  </p>
                </div>
                <div class="ae-grid__item--alt8 item-lg-6">
                  <span class="ae-u-boldest">3.</span>
                  <p class="au-mt-1">
                    Sed quam odio, blandit sit amet dapibus id, aliquet
                    sollicitudin enim.{" "}
                  </p>
                  <span class="ae-u-boldest">4.</span>
                  <p class="au-mt-1">
                    Morbi hendrerit laoreet lectus a fringilla.
                  </p>
                </div>
              </div>
            </div>
            <div class="ae-grid__item--alt item-sm--auto item-lg-3 item-lg--offset-1 item-lg--end">
              <img
                src={require("../../../resources/images/descarga (2).jpg")}
                alt="Urku Portfolio"
              />
            </div>
          </div>

          <div class="ae-container-fluid au-pt-4 au-pb-4">
            <div class="group-buttons">
              <a href="/" class="au-mt-2 arrow-button arrow-button--center">
                COMPRAR
                <span class="arrow-cont">
                  <svg>
                    <use href="assets/img/symbols.svg#arrow" />
                  </svg>
                </span>
              </a>
            </div>
          </div>
        </div>

        <div style={valor}>
          <div class="ae-grid__item item-lg-8 item-lg--offset-2">
            <h4 class="ae-u-bolder">pez 9</h4>
            <p class="ae-eta">
              Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
              officia deserunt mollit anim id est laborum.
            </p>
            <p class="ae-eta">
              Sed ut perspiciatis unde omnis iste natus error sit voluptatem
              accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
              quae ab illo inventore veritatis.
            </p>
            <div>
              {" "}
              <img
                src={require("../../../resources/images/descarga (2).jpg")}
                alt="Urku Portfolio"
              />
            </div>
          </div>

          <hr />
          <h3>Peces en venta xoxoxo</h3>

          <br />

          <div class="ae-grid--alt test">
            <div class="ae-grid__item--alt item-sm--auto item-sm--offset-1 item-lg--offset-0 item-sm--end item-sm--bottom">
              <div class="ae-grid ae-grid--alt8 au-xs-ta-center au-sm-ta-left">
                <div class="ae-grid__item--alt8 item-lg-6">
                  <span class="ae-u-boldest">1.</span>
                  <p class="au-mt-1">
                    Suspendisse ultricies tellus eget nisl molestie, quis
                    sagittis mauris placerat.
                  </p>
                  <span class="ae-u-boldest">2.</span>
                  <p class="au-mt-1">
                    Pellentesque et nulla pulvinar, bibendum quam ac, cursus
                    turpis.
                  </p>
                </div>
                <div class="ae-grid__item--alt8 item-lg-6">
                  <span class="ae-u-boldest">3.</span>
                  <p class="au-mt-1">
                    Sed quam odio, blandit sit amet dapibus id, aliquet
                    sollicitudin enim.{" "}
                  </p>
                  <span class="ae-u-boldest">4.</span>
                  <p class="au-mt-1">
                    Morbi hendrerit laoreet lectus a fringilla.
                  </p>
                </div>
              </div>
            </div>
            <div class="ae-grid__item--alt item-sm--auto item-lg-3 item-lg--offset-1 item-lg--end">
              <img
                src={require("../../../resources/images/descarga (2).jpg")}
                alt="Urku Portfolio"
              />
            </div>
          </div>

          <div class="ae-container-fluid au-pt-4 au-pb-4">
            <div class="group-buttons">
              <a href="/" class="au-mt-2 arrow-button arrow-button--center">
                COMPRAR
                <span class="arrow-cont">
                  <svg>
                    <use href="assets/img/symbols.svg#arrow" />
                  </svg>
                </span>
              </a>
            </div>
          </div>
        </div>

        <div style={valor}>
          <div class="ae-grid__item item-lg-8 item-lg--offset-2">
            <h4 class="ae-u-bolder">pez 10</h4>
            <p class="ae-eta">
              Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
              officia deserunt mollit anim id est laborum.
            </p>
            <p class="ae-eta">
              Sed ut perspiciatis unde omnis iste natus error sit voluptatem
              accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
              quae ab illo inventore veritatis.
            </p>
            <div>
              {" "}
              <img
                src={require("../../../resources/images/descarga (2).jpg")}
                alt="Urku Portfolio"
              />
            </div>
          </div>

          <hr />
          <h3>Peces en venta xoxoxo</h3>

          <br />

          <div class="ae-grid--alt test">
            <div class="ae-grid__item--alt item-sm--auto item-sm--offset-1 item-lg--offset-0 item-sm--end item-sm--bottom">
              <div class="ae-grid ae-grid--alt8 au-xs-ta-center au-sm-ta-left">
                <div class="ae-grid__item--alt8 item-lg-6">
                  <span class="ae-u-boldest">1.</span>
                  <p class="au-mt-1">
                    Suspendisse ultricies tellus eget nisl molestie, quis
                    sagittis mauris placerat.
                  </p>
                  <span class="ae-u-boldest">2.</span>
                  <p class="au-mt-1">
                    Pellentesque et nulla pulvinar, bibendum quam ac, cursus
                    turpis.
                  </p>
                </div>
                <div class="ae-grid__item--alt8 item-lg-6">
                  <span class="ae-u-boldest">3.</span>
                  <p class="au-mt-1">
                    Sed quam odio, blandit sit amet dapibus id, aliquet
                    sollicitudin enim.{" "}
                  </p>
                  <span class="ae-u-boldest">4.</span>
                  <p class="au-mt-1">
                    Morbi hendrerit laoreet lectus a fringilla.
                  </p>
                </div>
              </div>
            </div>
            <div class="ae-grid__item--alt item-sm--auto item-lg-3 item-lg--offset-1 item-lg--end">
              <img
                src={require("../../../resources/images/descarga (2).jpg")}
                alt="Urku Portfolio"
              />
            </div>
          </div>

          <div class="ae-container-fluid au-pt-4 au-pb-4">
            <div class="group-buttons">
              <a href="/" class="au-mt-2 arrow-button arrow-button--center">
                COMPRAR
                <span class="arrow-cont">
                  <svg>
                    <use href="assets/img/symbols.svg#arrow" />
                  </svg>
                </span>
              </a>
            </div>
          </div>
        </div>

        <div style={valor}>
          <div class="ae-grid__item item-lg-8 item-lg--offset-2">
            <h4 class="ae-u-bolder">pez 11</h4>
            <p class="ae-eta">
              Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
              officia deserunt mollit anim id est laborum.
            </p>
            <p class="ae-eta">
              Sed ut perspiciatis unde omnis iste natus error sit voluptatem
              accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
              quae ab illo inventore veritatis.
            </p>
            <div>
              {" "}
              <img
                src={require("../../../resources/images/descarga (2).jpg")}
                alt="Urku Portfolio"
              />
            </div>
          </div>

          <hr />
          <h3>Peces en venta xoxoxo</h3>

          <br />

          <div class="ae-grid--alt test">
            <div class="ae-grid__item--alt item-sm--auto item-sm--offset-1 item-lg--offset-0 item-sm--end item-sm--bottom">
              <div class="ae-grid ae-grid--alt8 au-xs-ta-center au-sm-ta-left">
                <div class="ae-grid__item--alt8 item-lg-6">
                  <span class="ae-u-boldest">1.</span>
                  <p class="au-mt-1">
                    Suspendisse ultricies tellus eget nisl molestie, quis
                    sagittis mauris placerat.
                  </p>
                  <span class="ae-u-boldest">2.</span>
                  <p class="au-mt-1">
                    Pellentesque et nulla pulvinar, bibendum quam ac, cursus
                    turpis.
                  </p>
                </div>
                <div class="ae-grid__item--alt8 item-lg-6">
                  <span class="ae-u-boldest">3.</span>
                  <p class="au-mt-1">
                    Sed quam odio, blandit sit amet dapibus id, aliquet
                    sollicitudin enim.{" "}
                  </p>
                  <span class="ae-u-boldest">4.</span>
                  <p class="au-mt-1">
                    Morbi hendrerit laoreet lectus a fringilla.
                  </p>
                </div>
              </div>
            </div>
            <div class="ae-grid__item--alt item-sm--auto item-lg-3 item-lg--offset-1 item-lg--end">
              <img
                src={require("../../../resources/images/descarga (2).jpg")}
                alt="Urku Portfolio"
              />
            </div>
          </div>

          <div class="ae-container-fluid au-pt-4 au-pb-4">
            <div class="group-buttons">
              <a href="/" class="au-mt-2 arrow-button arrow-button--center">
                COMPRAR
                <span class="arrow-cont">
                  <svg>
                    <use href="assets/img/symbols.svg#arrow" />
                  </svg>
                </span>
              </a>
            </div>
          </div>
        </div>

        <div style={valor}>
          <div class="ae-grid__item item-lg-8 item-lg--offset-2">
            <h4 class="ae-u-bolder">pez 12</h4>
            <p class="ae-eta">
              Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
              officia deserunt mollit anim id est laborum.
            </p>
            <p class="ae-eta">
              Sed ut perspiciatis unde omnis iste natus error sit voluptatem
              accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
              quae ab illo inventore veritatis.
            </p>
            <div>
              {" "}
              <img
                src={require("../../../resources/images/descarga (2).jpg")}
                alt="Urku Portfolio"
              />
            </div>
          </div>

          <hr />
          <h3>Peces en venta xoxoxo</h3>

          <br />

          <div class="ae-grid--alt test">
            <div class="ae-grid__item--alt item-sm--auto item-sm--offset-1 item-lg--offset-0 item-sm--end item-sm--bottom">
              <div class="ae-grid ae-grid--alt8 au-xs-ta-center au-sm-ta-left">
                <div class="ae-grid__item--alt8 item-lg-6">
                  <span class="ae-u-boldest">1.</span>
                  <p class="au-mt-1">
                    Suspendisse ultricies tellus eget nisl molestie, quis
                    sagittis mauris placerat.
                  </p>
                  <span class="ae-u-boldest">2.</span>
                  <p class="au-mt-1">
                    Pellentesque et nulla pulvinar, bibendum quam ac, cursus
                    turpis.
                  </p>
                </div>
                <div class="ae-grid__item--alt8 item-lg-6">
                  <span class="ae-u-boldest">3.</span>
                  <p class="au-mt-1">
                    Sed quam odio, blandit sit amet dapibus id, aliquet
                    sollicitudin enim.{" "}
                  </p>
                  <span class="ae-u-boldest">4.</span>
                  <p class="au-mt-1">
                    Morbi hendrerit laoreet lectus a fringilla.
                  </p>
                </div>
              </div>
            </div>
            <div class="ae-grid__item--alt item-sm--auto item-lg-3 item-lg--offset-1 item-lg--end">
              <img
                src={require("../../../resources/images/descarga (2).jpg")}
                alt="Urku Portfolio"
              />
            </div>
          </div>

          <div class="ae-container-fluid au-pt-4 au-pb-4">
            <div class="group-buttons">
              <a href="/" class="au-mt-2 arrow-button arrow-button--center">
                COMPRAR
                <span class="arrow-cont">
                  <svg>
                    <use href="assets/img/symbols.svg#arrow" />
                  </svg>
                </span>
              </a>
            </div>
          </div>
        </div>

        <div style={valor}>
          <div class="ae-grid__item item-lg-8 item-lg--offset-2">
            <h4 class="ae-u-bolder">pez 13</h4>
            <p class="ae-eta">
              Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
              officia deserunt mollit anim id est laborum.
            </p>
            <p class="ae-eta">
              Sed ut perspiciatis unde omnis iste natus error sit voluptatem
              accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
              quae ab illo inventore veritatis.
            </p>
            <div>
              {" "}
              <img
                src={require("../../../resources/images/descarga (2).jpg")}
                alt="Urku Portfolio"
              />
            </div>
          </div>

          <hr />
          <h3>Peces en venta xoxoxo</h3>

          <br />

          <div class="ae-grid--alt test">
            <div class="ae-grid__item--alt item-sm--auto item-sm--offset-1 item-lg--offset-0 item-sm--end item-sm--bottom">
              <div class="ae-grid ae-grid--alt8 au-xs-ta-center au-sm-ta-left">
                <div class="ae-grid__item--alt8 item-lg-6">
                  <span class="ae-u-boldest">1.</span>
                  <p class="au-mt-1">
                    Suspendisse ultricies tellus eget nisl molestie, quis
                    sagittis mauris placerat.
                  </p>
                  <span class="ae-u-boldest">2.</span>
                  <p class="au-mt-1">
                    Pellentesque et nulla pulvinar, bibendum quam ac, cursus
                    turpis.
                  </p>
                </div>
                <div class="ae-grid__item--alt8 item-lg-6">
                  <span class="ae-u-boldest">3.</span>
                  <p class="au-mt-1">
                    Sed quam odio, blandit sit amet dapibus id, aliquet
                    sollicitudin enim.{" "}
                  </p>
                  <span class="ae-u-boldest">4.</span>
                  <p class="au-mt-1">
                    Morbi hendrerit laoreet lectus a fringilla.
                  </p>
                </div>
              </div>
            </div>
            <div class="ae-grid__item--alt item-sm--auto item-lg-3 item-lg--offset-1 item-lg--end">
              <img
                src={require("../../../resources/images/descarga (2).jpg")}
                alt="Urku Portfolio"
              />
            </div>
          </div>

          <div class="ae-container-fluid au-pt-4 au-pb-4">
            <div class="group-buttons">
              <a href="/" class="au-mt-2 arrow-button arrow-button--center">
                COMPRAR
                <span class="arrow-cont">
                  <svg>
                    <use href="assets/img/symbols.svg#arrow" />
                  </svg>
                </span>
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default InformacionPez2;
