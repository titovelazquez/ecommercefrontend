import React, { Component } from 'react';
import './InformacionPez.css';

class InformacionPez extends Component {

    constructor(props) {
        super(props);
        this.state = {
            seconds: 1,
            image: require('../../../resources/images/1.jpg'),
            peces: [1, 2, 3]
        };
    }
    tick() {
        this.setState(state => ({
            seconds: state.seconds + 1,
            image: require('../../../resources/images/' + state.peces[this.state.seconds] + '.jpg' )
        }));
        
        if (this.state.seconds > 2) {
            this.state.seconds = 0;
        }
    }
    componentDidMount() {
        this.interval = setInterval(() => this.tick(), 3000);
    }

    render() {
        return (
            <div>
                <div>
                    <img src={this.state.image} />
                </div>
            </div>
        );
    }
}

export default InformacionPez;