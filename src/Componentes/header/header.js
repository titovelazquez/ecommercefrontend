import React, { Component } from "react";
import { Link } from "react-router-dom";

import "./header.css";

export class Header extends Component {
  render() {
    return (
      <header class="navigation">
        <nav className="nav navbar navbar-expand-lg navbar-light bg-light">
          <Link className="navbar-brand" to="/">
            Bubbles
          </Link>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon" />
          </button>
          <div className="collapse navbar-collapse" id="navbarNavDropdown">
            <ul className="navbar-nav">
              <li className="nav-item">
                <Link className="nav-link" to="/">
                  <span className="glyphicon glyphicon-home nav-link">  Home</span>
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/pecesEnVenta">
                  <span className="glyphicon glyphicon-shopping-cart nav-link">  Carrito</span>
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/nosotros">
                  <span className="glyphicon glyphicon-globe nav-link"> Nosotros</span>
                </Link>
              </li>
              <li className="nav-item dropdown">
                <a className="nav-link" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                  <span className="glyphicon glyphicon-list nav-link"> otros.. </span>
                </a>
                <div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                  <Link className="dropdown-item" to="#">
                    Action
                  </Link>
                  <Link className="dropdown-item" to="#">
                    Another action
                  </Link>
                  <Link className="dropdown-item" to="#">
                    Something else here
                  </Link>
                </div>
              </li>
            </ul>
          </div>
          <div>
            <ul className="navbar-nav">
              <li className="nav-item">
                <Link className="nav-link" to="login">
                 <span class="glyphicon glyphicon-log-in nav-link" aria-hidden="true"></span> log-in
                </Link>
              </li>
            </ul>
          </div>
        </nav>
      </header>
    );
  }
}

export default Header;
