import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./footer.css";

class Footer extends Component {
  render() {
    return (
      <footer className="container-fluid rk-footer">
        <div className="ae-grid ae-grid--collapse">
          <div className="ae-grid__item item-lg-4 au-xs-ta-center au-lg-ta-left">
            <ul className="rk-menu rk-footer-menu">
              <li className="rk-menu__item">
                <Link to="/" className="rk-menu__link">
                  <span className="glyphicon glyphicon-home nav-link"> Inicio</span>
                </Link>
              </li>
              <li className="rk-menu__item">
                <a href="documentation.html" className="rk-menu__link">
                  Horarios
                </a>
              </li>
              <li className="rk-menu__item">
                <a href="contact.html" className="rk-menu__link">
                  Contactanos
                </a>
              </li>
            </ul>
            
          </div>
          <div className="ae-grid__item item-lg-4 au-xs-ta-center">
            <a
              href="https://www.facebook.com/Bubbles-México-299567027348579/"
              className="rk-social-btn "
            >
              <svg>
                <use href="assets/img/symbols.svg#icon-facebook" />
              </svg>
            </a>
            <a href="infoPaginaService.info.twitter" className="rk-social-btn ">
              <svg>
                <use href="assets/img/symbols.svg#icon-twitter" />
              </svg>
            </a>
            <a
              href="infoPaginaService.info.pinterest"
              className="rk-social-btn "
            >
              <svg>
                <use href="assets/img/symbols.svg#icon-pinterest" />
              </svg>
            </a>
            <a href="infoPaginaService.info.tublr" className="rk-social-btn ">
              <svg>
                <use href="assets/img/symbols.svg#icon-tumblr" />
              </svg>
            </a>
          </div>
          <div className="ae-grid__item item-lg-4 au-xs-ta-center au-lg-ta-right">
            <p className="rk-footer__text rk-footer__contact ">
              <span className="ae-u-bold">Email: bubblesmexico@gmail.com </span>
              <span className="ae-u-bolder">
                <a href="#0" className="rk-dark-color " />
              </span>
            </p>
            <p className="rk-footer__text rk-footer__copy ">
              {" "}
              <span className="ae-u-bold">© </span>
              <span className="ae-u-bolder"> </span>All Right Reserved.
            </p>
           
              
            
          </div>
        </div>
      </footer>
    );
  }
}

export default Footer;
