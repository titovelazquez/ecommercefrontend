import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.css';
import { Inicio } from './Componentes/body/Inicio';
import { Login } from './Componentes/body/login';
import { Nosotros } from './Componentes/body/Nosotros/nosotros';
import {Encuentranos} from './Componentes/body/Encuentranos';
import  Header  from './Componentes/header/header';
import Footer from './Componentes/footer/footer';
import Link from './Componentes/body/link';
import { InformacionPez2 } from './Componentes/body/InformacionPez/InformacionPez2';
import PecesEnVenta from './Componentes/body/PecesEnVenta/PecesEnVenta';

class App extends Component {
  render() {
    return (
       <Router>
         <div>
           <Header></Header>
           <Switch>
             <Route path="/" component={Inicio} exact />
             <Route path="/link" component={Link} exact />
             <Route path="/login" component={Login} exact />
             <Route path="/nosotros" component={Nosotros} exact />
             <Route path="/encuentranos" component={Encuentranos} exact />
             <Route path="/pecesEnVenta" component={PecesEnVenta} exact />
             <Route path="/informacionPez2" component={InformacionPez2} exact />
           </Switch>
           <Footer></Footer>
         </div>
       </Router>
    );
  }
}

export default App;
